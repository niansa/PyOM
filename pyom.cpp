#include "pyom.hpp"



namespace PyOM {
#ifdef PYOM_ENABLE_SERIALIZE
void object_container::serialize(std::ostream &stream, unsigned int depth) {
    const char types[] = {'N', 'L', 'D', 'S', 'I'};
    stream << '(' << types[index()] << ") ";
    switch (type_index(index())) {
    case type_index::none: stream << "None"; break;
    case type_index::string: stream << get_value<type_index::string>(); break;
    case type_index::integer: stream << get_value<type_index::integer>(); break;
    case type_index::list: {
        stream << '\n';
        for (const auto& value : get_value<type_index::list>()) {
            stream << std::string(depth, ' ');
            value->serialize(stream, depth+4);
            stream << '\n';
        }
    } break;
    case type_index::dictionary: {
        stream << '\n';
        for (const auto& [key, value] : get_value<type_index::dictionary>()) {
            stream << std::string(depth, ' ') << key << ": ";
            value->serialize(stream, depth+key.size()+6);
            stream << '\n';
        }
    } break;
    }
}
#endif


void parser::parse_list(consumer_pointer it) {
    *get_current_object() = list{};
    list& fres = get_current_object()->get_value<type_index::list>();

    // Skip over [
    it->next();

    // Build list
    for (;;) {
        // Stop on ']'
        strip(it);
        if (*it == ']')
            break;

        // Parse child
        make_child_object();
        parse_object(it);
        fres.push_back(std::move(get_current_object()));
        pop_child_object();

        // Skip comma
        strip(it);
        if (*it != ']') {
            PYOM_ASSERT("Bad value separator in list", *it, *it == ',');
            it->next();
        } else {
            break;
        }
    }

    // Skip over ']'
    it->next();
}

void parser::parse_dictionary(consumer_pointer it) {
    *get_current_object() = dictionary{};
    dictionary& fres = get_current_object()->get_value<type_index::dictionary>();

    // Skip over {
    it->next();

    // Build dictionary
    for (;;) {
        // Stop on ']'
        strip(it);
        if (*it == '}')
            break;

        // Parse child name
        make_child_object();
        parse_string(it);
        string key = std::move(get_current_object()->get_value<type_index::string>());
        PYOM_ASSERT("Empty key in dictionary", *it, !key.empty());

        // Skip over ':'
        strip(it);
        PYOM_ASSERT("Bad separator between key and value in dictionary", *it, *it == ':');
        it->next();
        strip(it);

        // Parse child
        parse_object(it);
        fres.emplace(std::move(key), std::move(get_current_object()));
        pop_child_object();

        // Skip comma
        strip(it);
        if (*it != '}') {
            PYOM_ASSERT("Bad key/value pair separator in dictionary", *it, *it == ',');
            it->next();
        } else {
            break;
        }
    }

    // Skip over ']'
    it->next();
}

void parser::parse_string(consumer_pointer it) {
    *get_current_object() = string{};
    string& fres = get_current_object()->get_value<type_index::string>();

    // Get quotation marks used here
    const char quotes = *it;

    // Build string
    for (bool after_bs = false;;) {
        it->next();
        if (*it == '\\') {
            after_bs = true;
            continue;
        }
        char c = *it;
        if (after_bs) {
            after_bs = false;
            switch (c) {
            case 'n': c = '\n'; break;
            case 'r': c = '\r'; break;
            case 't': c = '\t'; break;
            }
        } else if (c == quotes) {
            break;
        }
        fres.push_back(c);
    }

    // Move past final quotation marks
    it->next();
}

void parser::parse_integer(consumer_pointer it) {
    *get_current_object() = integer{};
    int& fres = get_current_object()->get_value<type_index::integer>();

    // Check if interger is negative
    bool is_neg = *it == '-';
    if (is_neg)
        it->next();

    // Build integer
    while (*it >= '0' && *it <= '9') {
        fres = fres * 10 + (*it - '0');
        it->next();
    }

    // Negate integer if appropriate
    if (is_neg)
        fres = -fres;
}

void parser::parse_object(consumer_pointer it) {
    // Run parser based on type
    switch (get_type(*it)) {
    case type_index::none: parse_null(it); break;
    case type_index::list: parse_list(it); break;
    case type_index::dictionary: parse_dictionary(it); break;
    case type_index::string: parse_string(it); break;
    case type_index::integer: parse_integer(it); break;
    }
}

std::shared_ptr<object_container> parser::parse(PyOM::parser::consumer &consumer_) {
    // Get consumer pointer
    consumer_pointer consumer =
#ifndef PYOM_ENABLE_STATIC_CONSUMER
            consumer_;
#else
            &consumer_;
#endif

    // Reset consumer
    consumer->reset();

    // Strip initial input
    strip(consumer);

    // Strip down stack
    while (stack.size() != 1)
        stack.pop();

    // Set currently parsed object to root
    get_current_object() = root;

    // Parse
    parse_object(consumer);

    // Return root
    return root;
}
}
