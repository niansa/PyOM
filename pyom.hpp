#ifndef PYOM_HPP
#define PYOM_HPP

#include <string>
#include <string_view>
#include <stack>
#include <vector>
#include <unordered_map>
#include <variant>
#include <memory>
#ifdef PYOM_ENABLE_SERIALIZE
#include <iostream>
#endif
#ifdef PYOM_ENABLE_EXCEPTIONS
#include <stdexcept>
#endif


namespace PyOM {
// Asserts
#define PYOM_ASSERT_MESSAGE(message, currchar, ...) ("PyOM: In "+std::string(__FUNCTION__)+" assert (" #__VA_ARGS__ ") at character '"+std::string(1, (currchar))+"' has failed: " message)

#ifdef PYOM_ENABLE_EXCEPTIONS
struct assertion : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

#define PYOM_ASSERT(message, currchar, ...) if (!(__VA_ARGS__)) throw ::PyOM::assertion(PYOM_ASSERT_MESSAGE(message, currchar, __VA_ARGS__))
#else
#ifdef PYOM_ENABLE_ASSERT
#define PYOM_ASSERT(message, currchar, ...) if (!(__VA_ARGS__)) {std::cerr << "\n\n" << PYOM_ASSERT_MESSAGE(message, currchar, __VA_ARGS__) << std::endl; abort();} 0
#else
#define PYOM_ASSERT(message, currchar, ...) 0
#endif
#endif

// Base declarations
struct object_container;
using object_ptr = std::shared_ptr<object_container>;

// Python types
using list = std::vector<object_ptr>;
using dictionary = std::unordered_map<std::string, object_ptr>;
using string = std::string;
using integer = int;
using none = std::nullptr_t;
enum class type_index {
    none, list, dictionary, string, integer
};

// Object and object container with helper functions
using object = std::variant<none, list, dictionary, string, integer>;
struct object_container : public object {
    using object::object;

    /// Checks if object is of given type
    ///
    /// \returns `true` if object is currently of type `type` (template argument)
    template<typename type>
    bool has_type() const {
        return std::holds_alternative<type>(*this);
    }

    /// Checks if object is of given type
    ///
    /// \returns `true` if object is currently of type `type` (runtime argument)
    bool has_type(type_index type) const {
        return index() == int(type);
    }

    /// Gets constant reference to value
    ///
    /// \returns constant reference to value
    template<type_index type>
    const auto& get_value() const {
        return std::get<int(type)>(*this);
    }

    /// Gets reference to value
    ///
    /// \returns reference to value
    template<type_index type>
    auto& get_value() {
        return std::get<int(type)>(*this);
    }

#ifdef PYOM_ENABLE_SERIALIZE
    /// Serializes object
    ///
    /// This function serializes the object into a human-readable format for debugging purposes. It is NOT compatible with Python.
    void serialize(std::ostream& stream, unsigned depth = 4);
#endif
};

#ifndef PYOM_ENABLE_STATIC_CONSUMER
struct consumer_base {
    virtual ~consumer_base() {}

    /// Returns current character
    virtual operator char() = 0;
    /// Resets consumer
    ///
    /// Called whenever parsing restarts
    virtual void reset() {}
    /// "Goes to" next character
    ///
    /// Not allowed to return if there is none
    virtual void next() = 0;
};

#ifdef PYOM_ENABLE_EXTRA_CONSUMERS
struct cached_consumer_base : consumer_base{
    char current = 0;

    /// "Goes to" next character and returns it
    virtual char nextv() = 0;

    virtual operator char() final override {
        return current;
    }
    virtual void next() final override {
        current = nextv();
    }
};

struct iterator_consumer : consumer_base {
    std::string_view::const_iterator it;

    iterator_consumer() {}
    iterator_consumer(std::string_view::const_iterator it)
        : it(it) {}

    virtual operator char() final override {
        return *it;
    }
    virtual void next() final override {
        ++it;
    }
};

struct string_consumer final : iterator_consumer {
    std::string_view str;

    string_consumer() {}
    string_consumer(std::string_view::const_iterator str)
        : str(str) {}

    virtual void reset() override {
        it = str.cbegin();
    }
};
#endif
#endif

class parser {
#ifndef PYOM_ENABLE_STATIC_CONSUMER
    using consumer = std::unique_ptr<consumer_base>;
    using consumer_pointer = consumer&;
#else
    using consumer = static_consumer;
    using consumer_pointer = consumer*;
#endif

    std::shared_ptr<object_container> root;
    std::stack<std::shared_ptr<object_container>> stack;

    void make_child_object() {
        stack.push(std::make_shared<object_container>());
    }
    void pop_child_object() {
        stack.pop();
    }
    std::shared_ptr<object_container>& get_current_object() {
        return stack.top();
    }

    void parse_null(consumer_pointer it) {
        while ((*it >= 'A' || *it <= 'Z') || (*it >= 'a' || *it <= 'z'))
            it->next();
    }
    void parse_list(consumer_pointer it);
    void parse_dictionary(consumer_pointer it);
    void parse_string(consumer_pointer it);
    void parse_integer(consumer_pointer it);
    void parse_object(consumer_pointer it);

    void strip(consumer_pointer it) {
        // Strip off tabulators, spaces and newlines
        while (*it == ' ' || *it == '\t' || *it == '\n' || *it == '\r')
            it->next();
    }

public:
    parser() {
        make_child_object();
        root = get_current_object();
    }

    /// Gets object type given first character
    ///
    /// Given the first character of a serialized object, this function returns matching the type index
    ///
    /// \returns type_index of object or none if unidentified
    static constexpr type_index get_type(char first_char) {
        switch (first_char) {
            case '"': case '\'': return type_index::string;
            case '[': return type_index::list;
            case '{': return type_index::dictionary;
        }
        if (first_char >= '0' && first_char <= '9')
            return type_index::integer;
        return type_index::none;
    }

    /// Parse from consumer
    ///
    /// Resets and parses from the consumer
    std::shared_ptr<object_container> parse(consumer&);
};
}

#endif // PYOM_HPP
