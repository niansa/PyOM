#define PYOM_ENABLE_EXCEPTIONS
#define PYOM_ENABLE_SERIALIZE
#define PYOM_ENABLE_EXTRA_CONSUMERS

#include "pyom.hpp"

#include <iostream>
#include <memory>


int main() {
    using namespace PyOM;

    parser prs;
    std::unique_ptr<consumer_base> scons = std::make_unique<string_consumer>(R"(["abc", [4321, 'Hello', {"name": "Tuxifan", "birth_year": 2005, "languages": ["english", "german"]}, [7, 6, 5, 4]], 123])");
    auto om = prs.parse(scons);
#ifdef PYOM_ENABLE_SERIALIZE
    om->serialize(std::cout);
#else
    std::cout << om->get_value<type_index::list>()[0]->get_value<type_index::string>() << std::endl;
#endif
    asm("int3");
}
